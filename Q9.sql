USE practice;

--SELECT SUM(Item.item_price) FROM  Item WHERE category_id=1  
--SELECT SUM(Item.item_price) FROM  Item WHERE category_id=2  
--SELECT SUM(Item.item_price) FROM  Item WHERE category_id=3  
--GROUP BY
---Item_category.category_id


SELECT 
  Item_category.category_name,
  SUM(Item.item_price) AS total_price
FROM 
   Item_category
INNER JOIN  
   Item
ON 
   Item.category_id = Item_category.category_id
   
   
GROUP BY
   Item_category.category_id
   
   
ORDER BY
   total_price DESC

